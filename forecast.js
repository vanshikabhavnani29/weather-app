//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=36b19263e710dad766e867228de2a607

const key = "36b19263e710dad766e867228de2a607";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
    //console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error ' + response.status);
    }
}

//getForecast()
//.then(data => console.log(data))
//.catch(err => console.log(err));